# Project 2: TSP using GA
# By: Alexander Welch and Maryna Sevryukova
# 6/20/2023

from population import initial_population, rank_population, selection, mate
from genetic_operations import breed_population, mutate_population
from city import City
import random
import plot
import csv


def user_choice():
    """
    This function lets the user interact with a menu to perform actions related to city data.
    It allows to enter cities manually, generate random cities, work with the last entered cities and exit.
    After cities are defined or chosen, it runs a genetic algorithm function with the list of cities.
    """

    # List to hold city objects.
    city_list = []

    while True:
        print("Genetic Algorithm for Traveling Salesman Problem")
        print("-" * 48)
        print("Please choose one of the options: \n 1. Enter data manually \n 2. Choose random cities\n 3. Choose last "
              "cities\n 4. Exit")

        choice = input_with_validation("\nYour choice: ", int, lambda x: 1 <= x <= 4)

        # Option 1: Enter city coordinates manually.
        if choice == 1:
            num_cities = input_with_validation("\nHow many cities would you like to enter: ", int, lambda x: x > 0)
            city_list = manual_city_entry(num_cities)

        # Option 2: Generate random city coordinates.
        elif choice == 2:
            num_cities = input_with_validation("\nHow many cities would you like to have: ", int, lambda x: x > 0)
            city_list = random_city_entry(num_cities)

        # Option 3: Use the last entered city coordinates.
        elif choice == 3:
            if not city_list:
                print("\nNo previous cities found. Please choose a different option.")
                continue

        # Option 4: Exit the program.
        elif choice == 4:
            break

        pop_size, mutation_rate, proportion, generations, stagnation, stagnation_value = user_parameters()

        # After the city coordinates are determined, it runs a genetic algorithm.
        start(population=city_list, pop_size=pop_size, elite_size=int(proportion * pop_size),
              mutation_rate=mutation_rate, generations=generations, stagnation=stagnation,
              stagnation_value=stagnation_value)


def input_with_validation(prompt, type_, validate):
    """
    Asks for user input, checking the type of the input and its validation.

    Parameters:
    prompt (str): The message to display when asking for input.
    type_ (type): The desired type of the input.
    validate (func): A function that takes the input and returns True if it is valid, False otherwise.

    Returns:
    The validated user input.
    """
    while True:
        try:
            value = type_(input(prompt))
            # If the input is valid according to the validate function, return it.
            if validate(value):
                return value
            else:
                print("\nInvalid input. Please try again.")
        except ValueError:
            print("\nInvalid input. Please try again.")


def manual_city_entry(num_cities: int) -> list:
    """
    Collects user input for manual entry of city coordinates.

    Parameters:
    num_cities (int): The number of cities for which the user will provide coordinates.

    Returns:
    list: The list of cities with user-provided coordinates.
    """
    city_list = []
    for i in range(num_cities):
        x = int(input(f"Enter x-coordinate for city {i + 1}: "))
        y = int(input(f"Enter y-coordinate for city {i + 1}: "))

        # Add the city to the city list
        city_list.append(City(x=x, y=y))

    return city_list


def random_city_entry(num_cities: int) -> list:
    """
    Generates random city coordinates.

    Parameters:
    num_cities (int): The number of cities for which to generate random coordinates.

    Returns:
    list: The list of cities with random coordinates.
    """
    city_list = []
    for i in range(num_cities):
        # Generate random x and y coordinates for each city
        x = int(random.random() * 200)
        y = int(random.random() * 200)

        # Add the city to the city list
        city_list.append(City(x=x, y=y))

    return city_list


def user_parameters() -> tuple:
    """
    Collects user input for various parameters required for the genetic algorithm.

    Returns:
    tuple: The user-provided parameters (population size, mutation rate, proportion of new children, number of generations).
    """
    pop_size = input_with_validation("\nEnter the population size: ", int, lambda x: x > 0)

    mutation_rate = input_with_validation("\nEnter the mutation rate (0 - 1): ", float, lambda x: 0 <= x <= 1)

    proportion = input_with_validation("\nEnter the percentage of new children (0 - 100): ", float, lambda x: 0 <= x <= 100)

    generations = input_with_validation("\nEnter the total number of generations to run: ", int, lambda x: x > 0)

    stagnation = input_with_validation("\nWould you like to use stagnation as the stopping condition (yes or no): ",
                                       str.lower, lambda x: x in {"yes", "no"})

    stagnation_value = 0
    if stagnation == "yes":
        stagnation = True
        stagnation_value = input_with_validation(
            "\nEnter the number of consecutive generations with no improvement for stopping condition (must be less "
            "than the generations value): ",
            int,
            lambda x: 0 <= x < generations)
    else:
        stagnation = False

    return pop_size, mutation_rate, proportion / 100, generations, stagnation, stagnation_value


def start(population: list, pop_size: int, elite_size: int, mutation_rate: float,
          generations: int, stagnation: bool, stagnation_value: int) -> list:
    """
    Run the genetic algorithm.

    Parameters:
    population (list): The initial population.
    pop_size (int): The size of the population.
    elite_size (int): The number of elites.
    mutation_rate (float): The mutation rate.
    generations (int): The number of generations.

    Returns:
    list: The best route found by the algorithm.
    """
    population = initial_population(pop_size, population)
    ranked_population = rank_population(population)
    progress = [1 / ranked_population[0][1]]
    initial_distance = 1 / ranked_population[0][1]

    stagnation_count = 0
    new_value = 0
    best_value = 99999
    best_route = 0
    values = []

    for _ in range(generations):
        population = next_generation(population, elite_size, mutation_rate)
        ranked_population = rank_population(population)
        old_value = new_value
        new_value = 1 / ranked_population[0][1]
        progress.append(new_value)
        values.append(old_value)

        if new_value < best_value:
            best_value = new_value
            best_route = population[ranked_population[0][0]]

        if stagnation:
            if stagnation_value <= stagnation_count:
                print(f"\nStagnant after {len(progress)} iterations.")
                break
            elif old_value == new_value:
                stagnation_count += 1
            elif old_value != new_value:
                stagnation_count = 0

    plot.plot_progress(progress)

    with open('values.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["Generation", "Value"])
        for i, value in enumerate(values):
            writer.writerow([i, value])

    print("\nBest sequence of cities:")
    for i, city in enumerate(best_route):
        print(f"{i + 1}. City: ({city.x}, {city.y})")

    print(f"\nInitial distance: {initial_distance}")
    print(f"Best distance: {best_value}\n")

    return best_route


def next_generation(current_gen: list, elite_size: int, mutation_rate: float) -> list:
    """Creates the next generation of routes.

    Parameters:
    current_gen (list): The current generation.
    elite_size (int): The number of elites.
    mutation_rate (float): The mutation rate.

    Returns:
    list: The next generation.
    """
    # Evaluate the current population and rank individuals based on fitness score (distance)
    ranked_population = rank_population(current_gen)

    # Perform selection based on fitness scores and defined elite size
    selection_results = selection(ranked_population, elite_size)

    # Create a mating pool by selecting pairs of individuals from the current generation
    pool = mate(current_gen, selection_results)

    # Create a new generation of individuals by breeding the selected pairs from the mating pool.
    # Elite individuals may bypass this process to retain the best solutions
    children = breed_population(pool, elite_size)

    # Introduce mutations to the new generation of individuals at the defined mutation rate.
    # This introduces genetic diversity and aids in exploration of the solution space
    next_gen = mutate_population(children, mutation_rate)

    return next_gen


user_choice()
