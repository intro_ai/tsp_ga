class Fitness:
    def __init__(self, route: list):
        self.route = route
        self.distance = 0
        self.fitness = 0.0

    def route_distance(self) -> int:
        if self.distance == 0:
            path_distance = 0

            # Use zip function to iterate over pairs of cities
            for from_city, to_city in zip(self.route, self.route[1:] + self.route[:1]):
                path_distance += from_city.distance(to_city)

            self.distance = path_distance

        return self.distance

    def route_fitness(self) -> float:
        if self.fitness == 0:
            self.fitness = 1 / float(self.route_distance())

        return self.fitness
