from fitness import Fitness
import numpy as np
import city


def initial_population(pop_size: int, city_list: list) -> list:
    """Initialize a population of routes.

    Parameters:
    pop_size (int): The size of the population.
    city_list (list): List of cities to be used in creating the population.

    Returns:
    list: The generated population of routes.
    """
    # for pop_size number of items, create a random route of cities
    routes = [city.create_route(city_list) for _ in range(pop_size)]
    return routes


def rank_population(population: list) -> list:
    """Rank the routes in the population based on their fitness.

    Parameters:
    population (list): The population of routes.

    Returns:
    list: The population ranked by fitness.
    """
    # Use enumerate to iterate over routes along with their indices
    fitness_results = {i: Fitness(route).route_fitness() for i, route in enumerate(population)}

    # Sort and return the population by fitness
    sorted_results = sorted(fitness_results.items(), key=lambda x: x[1], reverse=True)
    return sorted_results


def selection(pop_ranked: list, elite_size: int) -> list:
    """Selects the most fit individuals and performs a roulette wheel selection.

    Parameters:
    pop_ranked (list): The ranked population.
    elite_size (int): The number of individuals to be selected based on fitness.

    Returns:
    list: The selected individuals.
    """
    # Create a list with the indices of the elite individuals
    selection_results = [pop_ranked[i][0] for i in range(elite_size)]

    # Calculate the:
    # 1. Total fitness of all individuals in the population
    # 2. Relative fitness of each individual
    # 3. Cumulative sum of relative fitness scores
    fitness_sum = sum([individual[1] for individual in pop_ranked])
    relative_fitness = [individual[1] / fitness_sum for individual in pop_ranked]
    probabilities = [sum(relative_fitness[:i + 1]) for i in range(len(relative_fitness))]

    # Perform roulette wheel selection for the rest of the population
    for _ in range(len(pop_ranked) - elite_size):
        # Generate a random number between 0 and 1
        pick = np.random.rand()

        # Select an individual to be part of the mating pool based on the roulette wheel selection
        for (i, individual) in enumerate(pop_ranked):
            if pick <= probabilities[i]:
                selection_results.append(individual[0])
                break

    return selection_results


def mate(population: list, selection_results: list) -> list:
    """Create a mating pool.

    Parameters:
    population (list): The population of routes.
    selection_results (list): The selected individuals.

    Returns:
    list: The mating pool.
    """
    # Use a list comprehension to create the mating pool
    pool = [population[index] for index in selection_results]

    return pool

