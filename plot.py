from matplotlib import pyplot as plt


def plot_progress(progress):
    """
    Plot the progress of the genetic algorithm across generations.

    Parameters:
    progress (list): List of distances (fitness scores) per generation.

    Returns:
    None
    """
    plt.style.use('bmh')  # Set the plot style to 'bmh'

    plt.figure(figsize=(10, 5))
    plt.plot(progress, color='orange', linestyle='-')  # Change the line color to red (#FF0000)
    plt.grid(True)

    plt.title('Progress of the Genetic Algorithm')
    plt.legend(['Distance'])
    plt.ylabel('Distance', color='#000000')  # Change the y-label color to black (#000000)
    plt.xlabel('Generation', color='#000000')  # Change the x-label color to black (#000000)

    plt.tick_params(colors='#000000')  # Change the color of the tick labels to black (#000000)

    ax = plt.gca()
    ax.spines['bottom'].set_color('#000000')  # Change the color of the bottom spine to black (#000000)
    ax.spines['top'].set_color('#000000')  # Change the color of the top spine to black (#000000)
    ax.spines['left'].set_color('#000000')  # Change the color of the left spine to black (#000000)
    ax.spines['right'].set_color('#000000')  # Change the color of the right spine to black (#000000)

    plt.savefig('genetic_algorithm_progress.png')

    plt.show()
