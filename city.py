import numpy as np
import random


class City:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def __repr__(self) -> str:
        return "(" + str(self.x) + "," + str(self.y) + ")"

    def distance(self, city: 'City') -> float:
        x_distance = abs(self.x - city.x)
        y_distance = abs(self.y - city.y)
        distance = np.sqrt((x_distance ** 2) + (y_distance ** 2))
        return distance


def create_route(city_list: list) -> list:
    """
    Creates a random route by shuffling the city list.
    Args:
        city_list: List of all the cities.

    Returns:
        A shuffled list of cities representing a route.
    """
    route = random.sample(city_list, len(city_list))
    return route
