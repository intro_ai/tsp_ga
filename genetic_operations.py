import random
import numpy as np


def breed(parent1, parent2):
    """Partially matched crossover (PMX) breeding function. A section of genes from
    parent1 is directly copied to the child. The remaining genes are filled in from
    parent2 in the order they appear, without duplicating any genes in the child.

    Parameters:
    parent1 (list): The first parent.
    parent2 (list): The second parent.

    Returns:
    list: The child resulting from the breed operation.
    """
    child = [None] * len(parent1)
    geneA = int(random.random() * len(parent1))
    geneB = int(random.random() * len(parent1))

    startGene = min(geneA, geneB)
    endGene = max(geneA, geneB)

    for i in range(startGene, endGene):
        child[i] = parent1[i]

    for i in range(len(parent2)):
        if parent2[i] not in child:
            for j in range(len(child)):
                if child[j] is None:
                    child[j] = parent2[i]
                    break

    return child


def breed_population(pool: list, elite_size: int) -> list:
    """Breed a population.

    Parameters:
    pool (list): The mating pool ranked in order of performance (elite at start).
    elite_size (int): The number of elites.

    Returns:
    list: The population after breeding.
    """
    # Create the list to store the children
    children = []

    # Keep the elites (start of pool)
    children.extend(pool[:elite_size])

    # Breed the remaining individuals in the pool
    for i in range(elite_size, len(pool)):
        child = breed(pool[i], pool[len(pool) - i - 1])
        children.append(child)

    return children


def mutate(individual, mutationRate):
    """Partially matched crossover (PMX) breeding function. A section of genes from
    parent1 is directly copied to the child. The remaining genes are filled in from
    parent2 in the order they appear, without duplicating any genes in the child.

    Parameters:
    individual (list): The individual to be mutated.
    mutation_rate (float): The probability of a gene undergoing mutation.

    Returns:
    list: The individual after mutation.
    """
    for swapped in range(len(individual)):
        if random.random() < mutationRate:
            swapWith = int(random.random() * len(individual))

            city1 = individual[swapped]
            city2 = individual[swapWith]

            # Scramble mutation
            scramble_range = individual[swapped:swapWith]
            random.shuffle(scramble_range)
            individual[swapped:swapWith] = scramble_range

    return individual


def mutate_population(population: list, mutation_rate: float) -> list:
    """Perform mutation operation on a population.

    Parameters:
    population (list): The population to be mutated.
    mutation_rate (float): The mutation rate.

    Returns:
    list: The population after mutation.
    """
    # For each individual in the population, add the result of mutation to list and return when done
    mutated_population = [mutate(individual, mutation_rate) for individual in population]
    return mutated_population

